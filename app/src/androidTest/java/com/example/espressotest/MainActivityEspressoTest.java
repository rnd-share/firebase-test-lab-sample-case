package com.example.espressotest;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class MainActivityEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureTextChangesWork() {
        // Type text and then press the button.
        Espresso.onView(ViewMatchers.withId(R.id.inputField))
                .perform(ViewActions.typeText("HELLO"), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.changeText)).perform(ViewActions.click());

        // Check that the text was changed.
        Espresso.onView(ViewMatchers.withId(R.id.inputField)).check(ViewAssertions.matches(ViewMatchers.withText("Lalala")));
    }

    @Test
    public void changeText_newActivity() {
        // Type text and then press the button.
        Espresso.onView(ViewMatchers.withId(R.id.inputField)).perform(ViewActions.typeText("NewText"),
                ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.switchActivity)).perform(ViewActions.click());

        // This view is in a different Activity, no need to tell Espresso.
        Espresso.onView(ViewMatchers.withId(R.id.resultView)).check(ViewAssertions.matches(ViewMatchers.withText("NewText")));
    }
}